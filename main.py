import csv
import nltk
import unidecode
from nltk.corpus import stopwords

generated = []
nltk.download('stopwords')

def create_mails():
  with open('in.csv') as csvfile:
    readCSV = csv.reader(csvfile, delimiter=',')
    for row in readCSV:
      name = row[0].lower()
      name = unidecode.unidecode(name)
      stop_words = set(stopwords.words("portuguese"))
      name_parts = list(filter(lambda word: word not in stop_words, name.split(' ')))
      temp_mails = generate_mails(name_parts,row[1])

      actual_mail = None

      for temp in temp_mails:
        if not any(temp in email for email in generated):
          actual_mail = temp
          break
        
      if actual_mail == None:
        for i in range(1, 10):
          for temp in temp_mails:
            temp = temp.replace('@', '{}@'.format(i), 1)
            
            if not any(temp in email for email in generated):
              actual_mail = temp
              break
          
          if actual_mail != None:
            break

      generated.append(actual_mail)
    
    save_mail(generated)

def generate_mails(names, domain):

  user_mails = []

  if len(names) <= 1:
    return

  #name.surname@domain.com
  mail = "{}.{}@{}".format(names[0], names[len(names) -1], domain)
  user_mails.append(mail)

  if(len(names) > 2):

    #name.middlename@domain.com
    for i in range(1, len(names) - 1):
      mail = "{}.{}@{}".format(names[0], names[i], domain)
      user_mails.append(mail)

    #name.m.surname@domain.com
    for i in range(1, len(names) - 1):
      mail = "{}.{}.{}@{}".format(names[0], names[i][:1], names[len(names) -1], domain)
      user_mails.append(mail)

  mail = "{}.{}@{}".format(names[0], names[len(names) -1][:1], domain)
  user_mails.append(mail)

  return user_mails

def save_mail(emails):
  with open('out.csv', 'w', newline='') as csvfile:
    spamwriter = csv.writer(csvfile, delimiter=' ',
        quotechar='|', quoting=csv.QUOTE_MINIMAL)
    for email in emails:
      spamwriter.writerow([email])

create_mails()