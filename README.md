# Generate Emails

Gerar possibilidades de e-mail apartir de uma lista de nomes

##### Features
  - Gerar e-mails automaticamente
  - Não conflitar entre e-mails gerados
  - Possibilidade de utilização com vários domínios diferentes

##### Instalação

###### Requisito (compatível apenas com Python 3+)

Clonar o projeto para seu ambiente
Instalar as dependências através do comando 
  ```bash
  pip3 install -r requirements.txt
  ```
Incluir um arquivo CSV que será utilizado como entrada das informações no sistema

###### Detalhes do arquivo
- Nome do arquivo (in.csv)
- Formato CSV
- Formato dos dados
```
Sábra Lyman,domain.com.br
Alysa Henderson,domain.com.br
Luella Durfey,domain.com.br
```

Executar através do comando 
```
python3 main.py
```

Após a execução será gerado um novo arquivo CSV (out.csv) com os e-mails